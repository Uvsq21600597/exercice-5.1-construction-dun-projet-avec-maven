package fr.uvsq.Account;





import static org.junit.Assert.*;


import org.junit.Test;


public class AccountTest {


	 @Test 
	public void CreateAccountTest(){
		 Account c= new Account(500);
		assertEquals(500,(int)c.CheckAccount());
		
		
		}
	
	
	@Test 
	public void TransferMoneyTest() {
		
		Account c1= new Account(100);
		Account c2= new Account(100);
		c1.transfer(50,c2);
		assertEquals(150,(int) c2.CheckAccount());
		
		}
	
	
}
